/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

#ifndef CDO_TASK_H
#define CDO_TASK_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>

namespace cdo
{

class Task
{
private:
  enum class State
  {
    SETUP,
    IDLE,
    JOB,
    DIE
  };

  std::function<void()> function;
  bool useFunction{ false };

  void *(*routine)(void *) = nullptr;
  void *arg = nullptr;
  void *result = nullptr;

  State state{ State::SETUP };
  std::thread thread;
  std::mutex workMutex;
  std::mutex bossMutex;
  std::condition_variable workCond;
  std::condition_variable_any bossCond;
  static void task(Task *taskInfo);

public:
  Task();
  ~Task();
  void doAsync(const std::function<void()> &_function);
  void start(void *(*task_routine)(void *), void *task_arg);
  void *wait();
};

}  // namespace cdo

#endif /* CDO_TASK_H */
