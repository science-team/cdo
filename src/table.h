#ifndef TABLE_H
#define TABLE_H

#include <string>

namespace cdo
{

int define_table(const std::string &tablearg);

}

#endif
