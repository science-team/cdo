#ifndef UTIL_FILES_H
#define UTIL_FILES_H

#include <sys/types.h>
#include <string>

namespace FileUtils
{
bool file_exists(const std::string &fileName);
bool user_file_overwrite(const std::string &fileName);
off_t size(const char *filename);
std::string gen_suffix(int filetype, int vlistID, const std::string &refenceName);
}  // namespace FileUtils

#endif
