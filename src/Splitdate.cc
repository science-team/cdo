/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Splitdate   splitdate        Split into dates
*/

#include <cdi.h>

#include "cdo_options.h"
#include "process_int.h"
#include "util_files.h"
#include "util_string.h"

class Splitdate : public Process
{
public:
  using Process::Process;
  inline static CdoModule module = {
    .name = "Splitdate",
    .operators = { { "splitdate", SplitdateHelp }, { "splitdatetime", SplitdateHelp } },
    .aliases = {},
    .mode = EXPOSED,     // Module mode: 0:intern 1:extern
    .number = CDI_BOTH,  // Allowed number type
    .constraints = { 1, OBASE, OnlyFirst },
  };
  inline static RegisterEntry<Splitdate> registration = RegisterEntry<Splitdate>(module);

  int SPLITDATE;
  CdoStreamID streamID1;
  int taxisID1;

  CdoStreamID streamID2 = CDO_STREAM_UNDEF;
  int taxisID2;
  int vlistID2;

  bool splitDate;
  bool haveConstVars;
  bool dataIsUnchanged;

  std::string fileSuffix;

  VarList varList1;
  FieldVector2D vars;
  Varray<double> array;

public:
  void
  init() override
  {

    dataIsUnchanged = data_is_unchanged();

    SPLITDATE = module.get_id("splitdate");

    auto operatorID = cdo_operator_id();
    splitDate = (operatorID == SPLITDATE);

    streamID1 = cdo_open_read(0);

    auto vlistID1 = cdo_stream_inq_vlist(streamID1);
    vlistID2 = vlistDuplicate(vlistID1);

    taxisID1 = vlistInqTaxis(vlistID1);
    taxisID2 = taxisDuplicate(taxisID1);
    vlistDefTaxis(vlistID2, taxisID2);

    varList1 = VarList(vlistID1);

    fileSuffix = FileUtils::gen_suffix(cdo_inq_filetype(streamID1), vlistID1, cdo_get_stream_name(0));

    //  if (! dataIsUnchanged)
    {
      auto gridsizemax = vlistGridsizeMax(vlistID1);
      if (vlistNumber(vlistID1) != CDI_REAL) gridsizemax *= 2;
      array.resize(gridsizemax);
    }

    haveConstVars = (varList1.numConstVars() > 0);
    if (haveConstVars)
      {
        int numVars = varList1.numVars();
        vars.resize(numVars);

        for (int varID = 0; varID < numVars; ++varID)
          {
            const auto &var = varList1.vars[varID];
            if (var.isConstant)
              {
                vars[varID].resize(var.nlevels);

                for (int levelID = 0; levelID < var.nlevels; ++levelID)
                  {
                    vars[varID][levelID].grid = var.gridID;
                    vars[varID][levelID].resize(var.gridsize);
                  }
              }
          }
      }
  }

  void
  run() override
  {
    int64_t vDate0 = -1;

    streamID2 = CDO_STREAM_UNDEF;
    int tsID2 = 0;
    int tsID = 0;

    while (true)
      {
        auto numFields = cdo_stream_inq_timestep(streamID1, tsID);
        if (numFields == 0) break;

        cdo_taxis_copy_timestep(taxisID2, taxisID1);

        auto vDateTime = taxisInqVdatetime(taxisID1);

        if (splitDate)
          {
            auto vDate = cdiDate_get(vDateTime.date);
            if (vDate != vDate0)
              {
                if (streamID2 != CDO_STREAM_UNDEF) cdo_stream_close(streamID2);

                vDate0 = vDate;
                tsID2 = 0;

                int year, month, day;
                cdiDate_decode(vDateTime.date, &year, &month, &day);
                auto fileName = cdo_get_obase() + string_format("%04d-%02d-%02d", year, month, day);
                if (fileSuffix.size() > 0) fileName += fileSuffix;

                if (Options::cdoVerbose) cdo_print("create file %s", fileName);

                streamID2 = cdo_open_write(fileName.c_str());
                cdo_def_vlist(streamID2, vlistID2);
              }
          }
        else
          {
            if (streamID2 != CDO_STREAM_UNDEF) cdo_stream_close(streamID2);

            tsID2 = 0;

            int year, month, day;
            cdiDate_decode(vDateTime.date, &year, &month, &day);
            int hour, minute, second, ms;
            cdiTime_decode(vDateTime.time, &hour, &minute, &second, &ms);
            auto fileName
                = cdo_get_obase() + string_format("%04d-%02d-%02dT%02d:%02d:%02d", year, month, day, hour, minute, second);
            if (fileSuffix.size() > 0) fileName += fileSuffix;

            if (Options::cdoVerbose) cdo_print("create file %s", fileName);

            streamID2 = cdo_open_write(fileName.c_str());
            cdo_def_vlist(streamID2, vlistID2);
          }

        cdo_def_timestep(streamID2, tsID2);

        if (tsID > 0 && tsID2 == 0 && haveConstVars)
          {
            int numVars = varList1.numVars();
            for (int varID = 0; varID < numVars; ++varID)
              {
                const auto &var = varList1.vars[varID];
                if (var.isConstant)
                  {
                    for (int levelID = 0; levelID < var.nlevels; ++levelID)
                      {
                        cdo_def_field(streamID2, varID, levelID);
                        auto numMissVals = vars[varID][levelID].numMissVals;
                        cdo_write_field(streamID2, vars[varID][levelID].vec_d.data(), numMissVals);
                      }
                  }
              }
          }

        for (int fieldID = 0; fieldID < numFields; ++fieldID)
          {
            auto [varID, levelID] = cdo_inq_field(streamID1);
            cdo_def_field(streamID2, varID, levelID);

            if (dataIsUnchanged && !(tsID == 0 && haveConstVars)) { cdo_copy_field(streamID2, streamID1); }
            else
              {
                size_t numMissVals;
                cdo_read_field(streamID1, array.data(), &numMissVals);
                cdo_write_field(streamID2, array.data(), numMissVals);

                if (tsID == 0 && haveConstVars)
                  {
                    const auto &var = varList1.vars[varID];
                    if (var.isConstant)
                      {
                        varray_copy(var.gridsize, array, vars[varID][levelID].vec_d);
                        vars[varID][levelID].numMissVals = numMissVals;
                      }
                  }
              }
          }

        tsID++;
        tsID2++;
      }
  }

  void
  close() override
  {
    if (streamID2 != CDO_STREAM_UNDEF) cdo_stream_close(streamID2);

    cdo_stream_close(streamID1);

    vlistDestroy(vlistID2);
  }
};
