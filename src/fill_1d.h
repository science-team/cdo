/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/
#ifndef FILL_1D_H
#define FILL_1D_H

#include "varray.h"

enum class FillMethod
{
  Undefined,
  Nearest,
  Linear,
  Forward,
  Backward
};

FillMethod string_to_fillmethod(const std::string &methodStr);

void fill_1d_nearest(int numValues, const Varray<double> &timeValues, Varray<double> &dataValues, double missval, int limit,
                     int maxGaps);
void fill_1d_linear(int numValues, const Varray<double> &timeValues, Varray<double> &dataValues, double missval, int limit,
                    int maxGaps);
void fill_1d_forward(int numValues, Varray<double> &dataValues, double missval, int limit, int maxGaps);
void fill_1d_backward(int numValues, Varray<double> &dataValues, double missval, int limit, int maxGaps);

#endif
