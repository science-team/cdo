/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Timcumsum    timcumsum         Cumulative sum over time
*/

#include <cdi.h>

#include "process_int.h"
#include "field_functions.h"

class Timcumsum : public Process
{
public:
  using Process::Process;
  inline static CdoModule module = {
    .name = "Timcumsum",
    .operators = { { "timcumsum", TimcumsumHelp } },
    .aliases = {},
    .mode = EXPOSED,     // Module mode: 0:intern 1:extern
    .number = CDI_BOTH,  // Allowed number type
    .constraints = { 1, 1, NoRestriction },
  };
  inline static RegisterEntry<Timcumsum> registration = RegisterEntry<Timcumsum>(module);

  CdoStreamID streamID1;
  int taxisID1;
  CdoStreamID streamID2;
  int taxisID2;

  Field field;
  FieldVector2D varsData1;

public:
  void
  init() override
  {
    operator_check_argc(0);

    streamID1 = cdo_open_read(0);

    auto vlistID1 = cdo_stream_inq_vlist(streamID1);
    auto vlistID2 = vlistDuplicate(vlistID1);

    taxisID1 = vlistInqTaxis(vlistID1);
    taxisID2 = taxisDuplicate(taxisID1);
    vlistDefTaxis(vlistID2, taxisID2);

    streamID2 = cdo_open_write(1);
    cdo_def_vlist(streamID2, vlistID2);

    auto gridsizemax = vlistGridsizeMax(vlistID1);
    if (vlistNumber(vlistID1) != CDI_REAL) gridsizemax *= 2;

    field.resize(gridsizemax);

    VarList varList1(vlistID1);
    field2D_init(varsData1, varList1, FIELD_VEC);
  }

  void
  run() override
  {
    int tsID = 0;
    while (true)
      {
        auto numFields = cdo_stream_inq_timestep(streamID1, tsID);
        if (numFields == 0) break;

        cdo_taxis_copy_timestep(taxisID2, taxisID1);
        cdo_def_timestep(streamID2, tsID);

        for (int fieldID = 0; fieldID < numFields; ++fieldID)
          {
            auto [varID, levelID] = cdo_inq_field(streamID1);

            auto &rvars1 = varsData1[varID][levelID];

            auto fieldsize = rvars1.size;

            if (tsID == 0)
              {
                cdo_read_field(streamID1, rvars1.vec_d.data(), &rvars1.numMissVals);
                if (rvars1.numMissVals)
                  for (size_t i = 0; i < fieldsize; ++i)
                    if (fp_is_equal(rvars1.vec_d[i], rvars1.missval)) rvars1.vec_d[i] = 0;
              }
            else
              {
                cdo_read_field(streamID1, field.vec_d.data(), &field.numMissVals);
                field.size = fieldsize;
                field.grid = rvars1.grid;
                field.missval = rvars1.missval;

                if (field.numMissVals)
                  for (size_t i = 0; i < fieldsize; ++i)
                    if (fp_is_equal(field.vec_d[i], rvars1.missval)) field.vec_d[i] = 0;

                field2_sum(rvars1, field);
              }

            cdo_def_field(streamID2, varID, levelID);
            cdo_write_field(streamID2, rvars1.vec_d.data(), rvars1.numMissVals);
          }

        tsID++;
      }
  }

  void
  close() override
  {
    cdo_stream_close(streamID2);
    cdo_stream_close(streamID1);
  }
};
