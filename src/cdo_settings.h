#ifndef CDO_SETTINGS_H
#define CDO_SETTINGS_H

#include <string>

namespace cdo
{

extern int netcdf_hdr_pad;

int evaluate_except_options(const std::string &arg);
int set_feenableexcept(int excepts);

void set_cdi_options();
void set_external_proj_func();

void signal_handler(int signo);

void set_digits(const std::string &arg);
void set_default_filetype(std::string filetypeString);
void set_default_datatype(const std::string &datatypeString);
void set_filterspec(const std::string &arg);
void set_compression_type(const std::string &arg);
void set_chunktype(const std::string &arg);

void evaluate_color_options(const std::string &arg);

void setup_openMP(int CDO_numThreads);

};  // namespace cdo

#endif
