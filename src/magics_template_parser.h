#ifndef MAGICS_TEMPLATE_PARSER_HH
#define MAGICS_TEMPLATE_PARSER_HH

int magics_template_parser(void *node);

int _set_magics_parameter_value(const char *param_name, const char *param_type, const char *param_value);

#endif
