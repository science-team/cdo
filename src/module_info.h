#ifndef MODULE_INFO_H
#define MODULE_INFO_H

#include <string>
#include <map>
#include <vector>

#include "operator_help.h"  // for CdoHelp

const std::string s_obase = "obase";
const std::string s_arbIn = "arbitrary";
const std::string s_filesOnly = "filesOnly";
const std::string s_onlyFirst = "onlyFirst";
const std::string s_noOutput = "noOutput";

struct ModListOptions
{
  bool printAll = false;
  bool operInfoRequested = false;
  std::map<const std::string, int> opt
      = { { s_obase, false }, { s_arbIn, false }, { s_filesOnly, false }, { s_onlyFirst, false }, { s_noOutput, false } };

  bool requested(const std::string &name);
  bool mod_info_requested();
  bool parse_request(const std::string &requestString);
};

std::string get_operator_description(const std::string &p_current_op_name, const std::vector<std::string> &p_help);
void operator_print_list(ModListOptions &p_modListOpt);

void cdo_print_help(const char **help);
void cdo_print_help(const std::string &help);
void cdo_print_help(const CdoHelp &p_help);
#endif
