/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

     Splityear  splityear       Split in years
     Splityear  splityearmon    Split in years and month
*/

#include <climits>
#include <cdi.h>

#include "cdo_options.h"
#include "process_int.h"
#include "util_files.h"
#include "util_string.h"

constexpr int MaxYears = 99999;

class Splityear : public Process
{
public:
  using Process::Process;
  inline static CdoModule module = {
    .name = "Splityear",
    .operators = { { "splityear", SplittimeHelp }, { "splityearmon", SplittimeHelp } },
    .aliases = {},
    .mode = EXPOSED,     // Module mode: 0:intern 1:extern
    .number = CDI_BOTH,  // Allowed number type
    .constraints = { 1, OBASE, OnlyFirst },
  };
  inline static RegisterEntry<Splityear> registration = RegisterEntry<Splityear>(module);

  int SPLITYEAR, SPLITYEARMON;
  int operatorID;
  CdoStreamID streamID1;
  Varray<int> cyear = Varray<int>(MaxYears, 0);
  std::string fileSuffix;
  int taxisID1;
  int taxisID2;
  int vlistID2;
  CdoStreamID streamID2 = CDO_STREAM_UNDEF;
  Varray<double> array;
  VarList varList1;
  bool haveConstVars;
  bool dataIsUnchanged;
  FieldVector2D vars;

public:
  void
  init() override
  {
    dataIsUnchanged = data_is_unchanged();

    SPLITYEAR = module.get_id("splityear");
    SPLITYEARMON = module.get_id("splityearmon");

    operatorID = cdo_operator_id();

    operator_check_argc(0);

    streamID1 = cdo_open_read(0);

    auto vlistID1 = cdo_stream_inq_vlist(streamID1);
    vlistID2 = vlistDuplicate(vlistID1);

    taxisID1 = vlistInqTaxis(vlistID1);
    taxisID2 = taxisDuplicate(taxisID1);
    vlistDefTaxis(vlistID2, taxisID2);

    fileSuffix = FileUtils::gen_suffix(cdo_inq_filetype(streamID1), vlistID1, cdo_get_stream_name(0));

    // if (! dataIsUnchanged)
    {
      auto gridsizemax = vlistGridsizeMax(vlistID1);
      if (vlistNumber(vlistID1) != CDI_REAL) gridsizemax *= 2;
      array.resize(gridsizemax);
    }

    varList1 = VarList(vlistID1);

    haveConstVars = (varList1.numConstVars() > 0);

    if (haveConstVars)
      {
        int numVars = varList1.numVars();
        vars.resize(numVars);

        for (int varID = 0; varID < numVars; ++varID)
          {
            const auto &var = varList1.vars[varID];
            if (var.isConstant)
              {
                vars[varID].resize(var.nlevels);

                for (int levelID = 0; levelID < var.nlevels; ++levelID)
                  {
                    vars[varID][levelID].grid = var.gridID;
                    vars[varID][levelID].resize(var.gridsize);
                  }
              }
          }
      }
  }

  void
  run() override
  {
    int ic = 0;
    int index1 = -INT_MAX;
    int index2;
    int year1 = -1, year2;
    int mon1 = -1, mon2;
    int tsID = 0;
    int tsID2 = 0;
    while (true)
      {
        auto numFields = cdo_stream_inq_timestep(streamID1, tsID);
        if (numFields == 0) break;

        cdo_taxis_copy_timestep(taxisID2, taxisID1);

        auto vDate = taxisInqVdatetime(taxisID1).date;
        int day;
        cdiDate_decode(vDate, &year2, &mon2, &day);

        if (operatorID == SPLITYEAR)
          {
            if (tsID == 0 || year1 != year2 || mon1 > mon2)
              {
                tsID2 = 0;

                ic = (year1 != year2) ? 0 : ic + 1;
                if (year2 >= 0 && year2 < MaxYears)
                  {
                    ic = cyear[year2];
                    cyear[year2]++;
                  }

                year1 = year2;

                if (streamID2 != CDO_STREAM_UNDEF) cdo_stream_close(streamID2);

                auto fileName = cdo_get_obase() + string_format("%04d", year1);
                if (ic > 0) fileName += string_format("_%d", ic + 1);
                if (fileSuffix.size() > 0) fileName += fileSuffix;

                if (Options::cdoVerbose) cdo_print("create file %s", fileName);

                streamID2 = cdo_open_write(fileName.c_str());
                cdo_def_vlist(streamID2, vlistID2);
              }
            mon1 = mon2;
          }
        else if (operatorID == SPLITYEARMON)
          {
            index2 = year2 * 100 + mon2;

            if (tsID == 0 || index1 != index2)
              {
                tsID2 = 0;

                index1 = index2;

                if (streamID2 != CDO_STREAM_UNDEF) cdo_stream_close(streamID2);

                auto fileName = cdo_get_obase() + string_format("%04d", index1);
                if (fileSuffix.size() > 0) fileName += fileSuffix;

                if (Options::cdoVerbose) cdo_print("create file %s", fileName);

                streamID2 = cdo_open_write(fileName.c_str());
                cdo_def_vlist(streamID2, vlistID2);
              }
          }

        cdo_def_timestep(streamID2, tsID2);

        if (tsID > 0 && tsID2 == 0 && haveConstVars)
          {
            int numVars = varList1.numVars();
            for (int varID = 0; varID < numVars; ++varID)
              {
                const auto &var = varList1.vars[varID];
                if (var.isConstant)
                  {
                    for (int levelID = 0; levelID < var.nlevels; ++levelID)
                      {
                        auto numMissVals = vars[varID][levelID].numMissVals;
                        cdo_def_field(streamID2, varID, levelID);
                        cdo_write_field(streamID2, vars[varID][levelID].vec_d.data(), numMissVals);
                      }
                  }
              }
          }

        for (int fieldID = 0; fieldID < numFields; ++fieldID)
          {
            auto [varID, levelID] = cdo_inq_field(streamID1);
            cdo_def_field(streamID2, varID, levelID);

            if (dataIsUnchanged && !(tsID == 0 && haveConstVars)) { cdo_copy_field(streamID2, streamID1); }
            else
              {
                size_t numMissVals;
                cdo_read_field(streamID1, array.data(), &numMissVals);
                cdo_write_field(streamID2, array.data(), numMissVals);

                if (tsID == 0 && haveConstVars)
                  {
                    const auto &var = varList1.vars[varID];
                    if (var.isConstant)
                      {
                        varray_copy(var.gridsize, array, vars[varID][levelID].vec_d);
                        vars[varID][levelID].numMissVals = numMissVals;
                      }
                  }
              }
          }

        tsID2++;
        tsID++;
      }
  }

  void
  close() override
  {
    cdo_stream_close(streamID1);
    cdo_stream_close(streamID2);
  }
};
