/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

#include "remap.h"

size_t
remap_search_cells(RemapSearch &rsearch, bool isReg2dCell, GridCell &gridCell, Varray<size_t> &searchIndices)
{
  if (rsearch.srcGrid->type == RemapGridType::Reg2D)
    return rsearch.gcs.reg2d->do_cellsearch(isReg2dCell, gridCell, searchIndices);
  else
    return rsearch.gcs.unstruct.do_cellsearch(isReg2dCell, gridCell, searchIndices);
}
