#include "bandit/bandit/bandit.h"
// BANDIT NEEDS TO BE INCLUDED FIRST!!!

#include <iostream>
#include <vector>

#include "../../src/modules.h"
#include "../../src/factory.h"
#include "../../src/cdo_module.h"

#include "test_module_list.h"

using namespace snowhouse;

go_bandit([]() {
  bandit::describe("Testing for registered module with alias", [&]() {
    bandit::it("has registered the module", [&]() {
      AssertThat(Factory::get().size(), Is().EqualTo(13ul));
    });
  });
  bandit::describe("Testing if the help was added", [&]() {
    bandit::it("and it can be retrieved", [&]() {
      const CdoHelp expected_container = { "dummy_help" };
      AssertThat(Factory::get_help("oper1-1"),
                 EqualsContainer(expected_container));
    });
  });
  bandit::describe("Testing the interface for alias and name retreval", [&]() {
    bandit::it("gets the right name for alias", [&]() {
      AssertThat(Factory::get_original(alias_name), Is().EqualTo("oper1-1"));
    });
  });
});

int
main(int argc, char **argv)
{

  int result = bandit::run(argc, argv);

  return result;
}
