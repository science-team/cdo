
#include "bandit/bandit/bandit.h"
// BANDIT NEEDS TO BE INCLUDED FIRST!!!

#include <iostream>
#include "../../src/modules.h"
#include "../../src/parser.h"

using namespace snowhouse;

go_bandit([]() {
  bandit::describe(
      "Testing operator name and argument extraction from command string",
      [&]() {
        bandit::it("returns the right name and argument with single "
                   "argument and usage of '-'",
                   [&]() {
                     std::string operName;
                     std::string operArgument;
                     Parser::Util::extract_name_and_argument("-test,arg", operName,
                                               operArgument);
                     AssertThat(operName, Is().EqualTo("test"));
                     AssertThat(operArgument, Is().EqualTo("arg"));
                   });
        bandit::it("does not cut off multiple arguments with no '-'", [&]() {
          std::string operName;
          std::string operArgument;
          Parser::Util::extract_name_and_argument("test,arg,arg2,arg3", operName,
                                    operArgument);
          AssertThat(operName, Is().EqualTo("test"));
          AssertThat(operArgument, Is().EqualTo("arg,arg2,arg3"));
        });
        bandit::it(
            "works with operators that have no arguments and are written "
            "without '-'",
            [&]() {
              std::string operName;
              std::string operArgument;
              Parser::Util::extract_name_and_argument("test", operName, operArgument);
              AssertThat(operName, Is().EqualTo("test"));
              AssertThat(operArgument, Is().EqualTo(""));
            });
        bandit::it("works with operators that have no arguments "
                   "and are written with '-'",
                   [&]() {
                     std::string operName;
                     std::string operArgument;
                     Parser::Util::extract_name_and_argument("-test", operName, operArgument);
                     AssertThat(operName, Is().EqualTo("test"));
                     AssertThat(operArgument, Is().EqualTo(""));
                   });
      });
});

int
main(int argc, char **argv)
{

  int result = bandit::run(argc, argv);

  return result;
}
