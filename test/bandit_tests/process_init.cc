
#include "bandit/bandit/bandit.h"
// BANDIT NEEDS TO BE INCLUDED FIRST!!!

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <iostream>

#include "../../src/cdo_module.h"
#include "../../src/process.h"
#include "../../src/factory.h"
#include "test_module_list.h"

using namespace snowhouse;

go_bandit([]() {
  cdo::progname = "process_init_test";
  std::vector<std::string> arguments = {};
  auto constructor_wrapper = Factory::find(alias_name);

  bandit::it("factory contains operators", [&]() {
    AssertThat(Factory::get().size(), Is().GreaterThan(0ul));
    bandit::it("found the alias", [&]() {
        auto &factory = Factory::get();
      AssertThat(constructor_wrapper != factory.end(),
                 Is().EqualTo(true));

      auto shared_process
          = constructor_wrapper->second.constructor(0, alias_name, arguments);

      bandit::it("assignes the origial of the alias as operatorName in init of "
                 "process",
                 [&]() {
                   AssertThat(shared_process->operatorName,
                              Is().EqualTo("oper1-1"));
                 });

      bandit::it(
          "creates the right prompt, discarding the alias in favor of the "
          "original name",
          [&]() {
            AssertThat(shared_process->prompt,
                       Is().EqualTo(std::string(cdo::progname) + "    "
                                    + shared_process->operatorName));
          });
    });
  });
});

int
main(int argc, char **argv)
{

  int result = bandit::run(argc, argv);

  return result;
}
