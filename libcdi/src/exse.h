#ifndef EXSE_H
#define EXSE_H

enum
{
  EXSE_PREC_FP16 = 2,
  EXSE_PREC_FP32 = 4,
  EXSE_PREC_FP64 = 8,
};

#endif
