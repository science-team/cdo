#!/usr/bin/make -f


export DH_VERBOSE=1

include /usr/share/debhelper/dh-fortran/fortran-support.mk

BUILDDIR:=debian/build

# The magic debhelper  rule
%:
	dh $@  --builddirectory=$(BUILDDIR) --with fortran_mod

DESTDIR=$(CURDIR)/debian/tmp/
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
LIBDIR:=/usr/lib/$(DEB_HOST_MULTIARCH)

# To enable all, uncomment following line
DEB_BUILD_MAINT_OPTIONS:= hardening=+all
DEB_CFLAGS_MAINT_APPEND:= -Wall -pedantic -fPIC
export DEB_BUILD_MAINT_OPTIONS
export DEB_CFLAGS_MAINT_APPEND DEB_CFLAGS_MAINT_APPEND

# Just use gfortran in the build
CPPFLAGS += -DgFortran

CPPFLAGS += -I/usr/include/cdTime

ARCH:=$(shell dpkg --print-architecture)

# On amd64don't do fPIE, only relro
ifeq ($(ARCH), amd64)
  export LDFLAGS= -Wl,-z,relro -Wl,-z,now
endif


BUILD_FLAGS = \
	--prefix=/usr --libdir=$(LIBDIR) \
	$(WITH_OPENMP) \
	--with-hdf5=/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial \
	--with-zlib=/usr \
	--with-netcdf=/usr  \
	--with-proj=/usr \
 	--with-fftw3 \
	--with-curl=/usr \
	--with-udunits2=/usr \
	--with-szlib \
	--with-libxml2=/usr \
	--with-eccodes=/usr \
	--with-magics=/usr \
	--enable-iso-c-interface \
	--enable-cdi-lib \
	--enable-hirlam-extensions \
	--disable-cgribex 

override_dh_auto_clean:
	dh_auto_clean	|| echo "distclean ok"
	rm -rf $(BUILDDIR)
	if test -f libcdi/src/cfortran.h.orig ; then ( rm libcdi/src/cfortran.h; mv libcdi/src/cfortran.h.orig libcdi/src/cfortran.h); fi

override_dh_auto_configure:
	[ -f libcdi/src/cfortran.h.orig ]|| ( cp libcdi/src/cfortran.h libcdi/src/cfortran.h.orig)
	ln -sf /usr/include/cfortran.h libcdi/src/cfortran.h
	for f in $(FC_DEFAULT)  ; do \
		dh_auto_configure --builddirectory=$(BUILDDIR) -- \
			FC=$$f $(BUILD_FLAGS)  \
			LIBS="-lm -ljpeg -lz -lcurl" LDFLAGS="$(LDFLAGS) -Wl,--as-needed" ; \
		done

override_dh_auto_build:
	dh_auto_build --builddirectory=$(BUILDDIR)
	# Ignore error here ; it mostly works
	- (cd contrib  && LC_ALL=C.UTF-8 ruby makecompl.rb )

override_dh_auto_install:
	dh_auto_install
	for d in debian/tmp/$(LIBDIR)/pkgconfig/cdi.pc $(BUILDDIR)/libcdi/src/pkgconfig/cdi_f2003.pc ; do \
		sed -e 's%${CURDIR}%/build/cdo%g' < $$d > debian/tmp/x ; \
		mv debian/tmp/x $$d ; \
	done
	mkdir -p $(DESTDIR)/usr/share/bash-completions/completions $(DESTDIR)//usr/share/zsh/functions/Completion/Linux
	cp contrib/cdoCompletion.bash $(DESTDIR)/usr/share/bash-completions/completions/cdo
	cp contrib/cdoCompletion.zsh $(DESTDIR)//usr/share/zsh/functions/Completion/Linux/_cdo
	find $(DESTDIR) -name '*.la' -delete -o -name '*.so' -delete -o -name '*.so.0' -delete
	mkdir -p $(DESTDIR)/$(LIBDIR)/fortran/gfortran
	mv $(DESTDIR)/$(LIBDIR)/libcdi_f2003.so.0.0.0  $(DESTDIR)/$(LIBDIR)//libcdi_f2003-gfortran.so.0.0.0
	mv $(DESTDIR)/$(LIBDIR)/libcdi_f2003.a         $(DESTDIR)/$(LIBDIR)/fortran/gfortran
	# patchelf  --set-soname libcdi_f2003-gfortran.so.0 $(DESTDIR)/$(LIBDIR)//libcdi_f2003-gfortran.so.0.0.0


override_dh_auto_test:
	# Need to define path to 'cdo' for python tests
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	export PATH=$(PATH):$(CURDIR)/src make check
endif
